<?php

class Todo {
    private $text;
    private $done;

    function __construct($text, $done){
        $this->text=$text;
        $this->done=$done;

    }
// les function doivent être à l'intérieur de la class (Todo)
    public function getText (){
        return $this->text;
    }

    public function setText ($text){
        $this->text = $text;
    }


    public function getDone (){
        return $this->done;
    }

    public function setDone ($done){
    $this->done = $done;
    }

    public function save(){
       $db = new DB();
       $db->save($this);
    }

    function writeData($todo, $filename = "database.txt") {
        $todo = serialize($todo);
        file_put_contents($filename, $todo);
    }

    // pour lire les données stockées dans le fichier "database.txt"
    // retourne False quand il n'y a pas de données
    function readData($filename = "database.txt") {
        $data = file_get_contents($filename);
        $data = unserialize($data);
        return $data;
    }

}

//$maVariable = new Todo ('coucou',true);
//$maVariable -> setText('Simplon');
?>