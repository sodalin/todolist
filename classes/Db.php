<?php

include(__DIR__.'/../config.php'); 

class DB {

    static function select(){
        // on se connecte a la BDD
        $bdd = connect();
        // on fait la requete SQL
        $request = $bdd->query("SELECT * FROM `todos`");
        // On recupere le resultat (tableau de todos)
        $todos = $request->fetchAll();
        // on retourne le tableau de todos
        return $todos;
    }

    static function insert($text, $done){
        $bdd = connect();
        $request = $bdd->prepare("INSERT INTO todos (text, done) VALUES (:text ,:done)");
        $request->execute(['text'=>$text, 'done'=> (int) $done]);
        }


    static function delete($id){
        $bdd = connect();
        $deleteId = $bdd->prepare("DELETE FROM todos WHERE id=:id");
        $deleteId->execute(['id'=>$id]);  
    }

    static function update($id, $text, $done){
        $bdd = connect();
        $changeText = $bdd->prepare("UPDATE todos SET text= $text, done= $done WHERE id = $id");
        $changeText->execute(['id'=>$id, 'text'=>$text, 'done'=>$done]);
    }
}
















    // private $filename;

    // function __construct($filename = "db/database.txt") {
    //     $this->filename = $filename;
    // }

    // public function save ($todo) {
    //     $arrayTodo = $this->getArrayTodo();

    //     // On ajoute la todo a la liste
    //     array_push($arrayTodo, $todo);

    //     $this->writeArrayTodo($arrayTodo);
    // }

    // public function remove ($todo) {
    //     $arrayTodo = $this->getArrayTodo();

    //     // On recherche la todo dans la liste
    //     foreach ($arrayTodo as $key => $item) {
    //         if ($item->getId() == $todo->getId()) {
    //             // On l'a trouvée, donc on la supprime
    //             unset($arrayTodo[$key]);
    //         }
    //     }

    //     $this->writeArrayTodo($arrayTodo);
    // }

    // public function getArrayTodo () {
    //     $data = [];
    //     if (file_exists($this->filename)) {
    //         $tmp = file_get_contents($this->filename);
    //         if (isset($tmp) && !empty($tmp)) {
    //             $data = unserialize($tmp);
    //         }
    //     }

    //     return $data;
    // }

    // private function writeArrayTodo ($arrayTodo) {
    //     $data = serialize($arrayTodo);
    //     file_put_contents($this->filename, $data);
    // }

    // public function clean () {
    //     file_put_contents($this->filename, "");
    // }



?>







